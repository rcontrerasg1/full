import { Routes } from '@angular/router';

import { FullscreenTableComponent } from './fullscreen-table/fullscreen-table.component';
import { PagingTableComponent } from './paging-table/paging-table.component';
import { FilterTableComponent } from './filter-table/filter-table.component';
import { FilterTableComponent1 } from './filter-table-1/filter-table-1.component';

export const TablesRoutes: Routes = [
  {
    path: '',
    children: [{
      path: 'fullscreen',
      component: FullscreenTableComponent,
      data: { title: 'Fullscreen', breadcrumb: 'FULLSCREEN' }
    }, {
      path: 'paging',
      component: PagingTableComponent,
      data: { title: 'Paging', breadcrumb: 'PAGING' }
    }, {
      path: 'filter',
      component: FilterTableComponent,
      data: { title: 'Filter', breadcrumb: 'FILTER' }
    }, {
      path: 'filter-1',
      component: FilterTableComponent1,
      data: { title: 'Filter-1', breadcrumb: 'FILTER-1' }
    }]
  }
];