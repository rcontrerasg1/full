import { Routes } from '@angular/router';
import { CrudNgxTableComponent } from './crud-ngx-table/crud-ngx-table.component';
import { CrudNgxTableComponent1 } from './crud-ngx-table-1/crud-ngx-table-1.component';

export const CrudsRoutes: Routes = [
  { 
    path: 'ngx-table', 
    component: CrudNgxTableComponent, 
    data: { title: 'NgX Table', breadcrumb: 'NgX Table' }
  },
  { 
    path: 'ngx-table1', 
    component: CrudNgxTableComponent1, 
    data: { title: 'NgX Table 1', breadcrumb: 'NgX Table 1' }
  }
];